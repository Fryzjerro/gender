/**
 * View Models used by Spring MVC REST controllers.
 */
package com.softdog.gender.web.rest.vm;
